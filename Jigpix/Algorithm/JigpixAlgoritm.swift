//
//  JigpixAlgoritm.swift
//  Jigpix
//
//  Created by Admin on 12/30/15.
//  Copyright © 2015 opes. All rights reserved.
//

import Foundation
import UIKit

//MARK: - Protocol JigpixAlgoritmDelegate
protocol JigpixAlgoritmDelegate {
    func jigpixAlgoritmDidStart(_ image: UIImage)
    func jigpixAlgoritmImageChanged(_ image: UIImage)
    func jigpixAlgoritmDidEnd(_ image: UIImage)
}

//MARK: - Enum Pieces
enum Pieces
{
    case small300
    case medium520
}

class JigpixAlgoritm
{
    internal static var puzzlePieces = Pieces.medium520;
    
    internal static var isBlackWhite = false;
    
    var delegate: JigpixAlgoritmDelegate? = nil;
    
    fileprivate var puzzle = Puzzle();
    
    fileprivate var outputImage: UIImage? = nil;
    fileprivate var codeImage: UIImage? = nil;
    fileprivate var shareImage: UIImage? = nil;
    
    fileprivate var selfieImage: UIImage? = nil;
    fileprivate var puzzleImage: UIImage? = nil;
    
    //MARK: - Consts
    internal static var COLS : Int { return JigpixAlgoritm.puzzlePieces == Pieces.small300 ? 15 : 20; }
    internal static var ROWS : Int { return JigpixAlgoritm.puzzlePieces == Pieces.small300 ? 20 : 26; }
    
    var stopWorking = false;
    
    //MARK: - Start
    func Start(_ source: UIImage, target: UIImage)
    {
        self.stopWorking = false;
     
        self.puzzleImage = source;
        self.selfieImage = target;
        
        self.outputImage = source;
        if(delegate != nil)
        {
            DispatchQueue.main.sync {
                self.delegate!.jigpixAlgoritmDidStart(self.outputImage!);
            }
        }
        
        self.puzzle.setup(JigpixAlgoritm.COLS, rows: JigpixAlgoritm.ROWS);
        
        self.puzzle.setFromImage(source, target: false);
        self.puzzle.setFromImage(target, target: true);
        
        self.puzzle.centralImortance = USettingsData.sharedInstance.CentralImportance;
        self.puzzle.powerFunc = USettingsData.sharedInstance.PowerFunction;
        
        var swapped = false;
        let refreshCnt = USettingsData.sharedInstance.RefreshRate
        var cnt = 0;
//        var i = 0;
        for _ in 0..<USettingsData.sharedInstance.MainCycle
        {
            if(self.stopWorking)
            {
                break;
            }
            
            if(self.puzzle.tryToSwapStuff())
            {
                swapped = true;
            }
//            i=i+1;
            cnt = cnt + 1;
            if(cnt >= refreshCnt)
            {
                self.changeImage();
                cnt = 0;
                if(swapped == false)
                {
                    break;
                }
                swapped = false;
                //print(i);
            }
        }
        
//        print("Main cycle cnt=\(i)");
        
        //SAVE OBJECT TO OUTPUT IMAGE
        self.saveOutputImage(true);
        
        //GENERATE CODE IMAGES PICTURE
        if(JigpixAlgoritm.puzzlePieces == Pieces.small300)
        {
            //TODO: prepare small codes image
            let cropImage = UIImage(named: "codes_small.png")!;
            self.generateCodeImage(cropImage);
        }
        else
        {
            let cropImage = UIImage(named: "codes.png")!;
            self.generateCodeImage(cropImage);
        }
        
        self.CreateImageForShare();
        
        if(delegate != nil)
        {
            DispatchQueue.main.sync {
                self.delegate!.jigpixAlgoritmDidEnd(self.outputImage!);
            }
        }
    }
    
    //MARK: - Fire image changed event with saving
    func changeImage()
    {
        self.saveOutputImage();
        
        if(delegate != nil)
        {
            DispatchQueue.main.async {
                self.delegate!.jigpixAlgoritmImageChanged(self.outputImage!);
            }
        }
    }
    
    //MARK: - Saves canvas to output image
    func saveOutputImage(_ last: Bool = false)
    {
        let COLS = self.puzzle.nPiecesHoriz;
        let ROWS = self.puzzle.nPiecesVert;
        let pieceSizeW = self.puzzle.smallResHoriz / self.puzzle.nPiecesHoriz;
        let centers = pieceSizeW * (last ? 3 : 1);//Int(self.puzzle.pieces[0].img!.size.width) * (last ? 2 : 1);
        let sizeResult = CGSize(width: centers * COLS, height: centers * ROWS);
        
        let opaque = false;
        let scale: CGFloat = 0;
        UIGraphicsBeginImageContextWithOptions(sizeResult, opaque, scale);
        let context = UIGraphicsGetCurrentContext();
        
        let rectangle = CGRect(x: 0, y: 0, width: sizeResult.width, height: sizeResult.height);
        context?.setFillColor(UIColor.white.cgColor);
        context?.addRect(rectangle);
        context?.drawPath(using: CGPathDrawingMode.fill);
        
        context?.setStrokeColor(UIColor.black.cgColor);
        context?.setLineWidth(1);
        for p in 0..<self.puzzle.nTotalPieces
        {
            let row = p / COLS;
            let col = p % COLS;
            
            var pieceImage = self.puzzle.pieces[p].img!;
            let degrees = self.puzzle.pieces[p].rotation * 90;
            if(degrees > 0)
            {
                pieceImage = pieceImage.rotate(degrees);
            }
            let drawRect = CGRect(x: col * centers, y: row * centers, width: centers, height: centers);
            pieceImage = last ? pieceImage.resizeImage2(CGSize(width: centers, height: centers)) : pieceImage;
            pieceImage.draw(in: drawRect);
            context?.addRect(drawRect);
            context?.drawPath(using: CGPathDrawingMode.stroke);
            
//            if last
//            {
//                print("NO=\(p + 1) ID=\(self.puzzle.pieces[p].id) R=\(self.puzzle.pieces[p].rotation)");
//            }
        }
        
        self.outputImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    //MARK: - Generate codes image
    fileprivate func generateCodeImage(_ cropImage: UIImage)
    {
        self.codeImage = nil;
        
        let centers = Int(cropImage.size.width) / JigpixAlgoritm.COLS;
        let size = CGSize(width: centers * JigpixAlgoritm.COLS, height: centers * JigpixAlgoritm.ROWS);
        
        // Setup our context
        let opaque = false;
        let scale: CGFloat = 0;
        UIGraphicsBeginImageContextWithOptions(size, opaque, scale);
        let context = UIGraphicsGetCurrentContext();
        
        let rectangle = CGRect(x: 0, y: 0, width: size.width, height: size.height);
        context?.setFillColor(UIColor.white.cgColor);
        context?.addRect(rectangle);
        context?.drawPath(using: CGPathDrawingMode.fill);
        
        //CGContextSetFillColorWithColor(context, UIColor.whiteColor().CGColor);
        context?.setStrokeColor(UIColor.black.cgColor);
        context?.setLineWidth(3);
        for p in 0..<self.puzzle.nTotalPieces
        {
            let row = p / self.puzzle.nPiecesHoriz;
            let col = p % self.puzzle.nPiecesHoriz;
            let piece = self.puzzle.pieces[p];
            let pieceNo = piece.id;
            
            let cropRect = CGRect(x: pieceNo % self.puzzle.nPiecesHoriz * centers,
                                  y: pieceNo / self.puzzle.nPiecesHoriz * centers,
                                  width: centers,
                                  height: centers);
            var image = cropImage.cropImage(cropRect);
            let angle = 90 * piece.rotation;
            if(angle != 0)
            {
                image = image.rotate(angle);//.imageRotatedByDegrees(CGFloat(angle), flip: false);
            }
            
            let drawRect = CGRect(x: col * centers, y: row * centers, width: centers, height: centers);
            image.draw(in: drawRect);
            context?.addRect(drawRect);
            context?.drawPath(using: CGPathDrawingMode.stroke);
        }
        
        // Drawing complete, retrieve the finished image and cleanup
        self.codeImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    //MARK: - Get codes image
    func GetCode() -> UIImage
    {
        return self.codeImage!;
    }
    
    //MARK: - Create image for share
    func CreateImageForShare()
    {
        let logo = UIImage(named: "jigpix")!;
        let code =  self.codeImage!;
        let puzzle = self.puzzleImage!.roundCorners();
        let selfie = self.selfieImage!.roundCorners();
        let result = self.outputImage!.roundCorners();
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 1120.0, height: 1408.0), false, 0);
        //        self.coordinateView.drawHierarchy(in: self.coordinateView.bounds, afterScreenUpdates: true);
        let image:UIImage = UIGraphicsGetImageFromCurrentImageContext()!;
        UIGraphicsEndImageContext();
        
        let heightOffset = image.size.height / 5.0;
        let sizeResult = CGSize(width: image.size.width, height: image.size.height + heightOffset);
        let opaque = false;
        let scale: CGFloat = 0;
        UIGraphicsBeginImageContextWithOptions(sizeResult, opaque, scale);
        let context = UIGraphicsGetCurrentContext();
        
        let borderRect = CGRect(x: 0, y: 0, width: sizeResult.width, height: sizeResult.height);
        context?.setFillColor(UIColor.gray.cgColor);
        context?.addRect(borderRect);
        context?.drawPath(using: CGPathDrawingMode.fill);
        
        let border: CGFloat = 40.0;
        let mainRect = CGRect(x: border, y: border, width: sizeResult.width - 2.0 * border, height: sizeResult.height - 2.0 * border);
        context?.setFillColor(UIColor.white.cgColor);
        context?.addRect(mainRect);
        context?.drawPath(using: CGPathDrawingMode.fill);
        
        let yOffset: CGFloat = 5.0;
        //Draw logo
        let logoRect = CGRect(x: border, y: yOffset + border + 10, width: image.size.width / 2.5 - border, height: heightOffset - yOffset - 30);
        logo.draw(in: logoRect);
        
        let smallImageWidth: CGFloat = (image.size.width - image.size.width / 2.5) / 3.0 - 40;
        
        //Draw puzzle
        let puzzleRect = CGRect(x: image.size.width / 2.5, y: yOffset + border, width: smallImageWidth, height: heightOffset - yOffset);
        puzzle.draw(in: puzzleRect);
        
        context?.setLineWidth(3.0);
        context?.setStrokeColor(UIColor.red.cgColor);
        
        //Draw plus
        context?.move(to: CGPoint(x: puzzleRect.origin.x + smallImageWidth + 2, y: heightOffset / 2.0 + border));
        context?.addLine(to: CGPoint(x: puzzleRect.origin.x + smallImageWidth + 2 + 20, y: heightOffset / 2.0 + border));
        context?.strokePath();
        context?.move(to: CGPoint(x: puzzleRect.origin.x + smallImageWidth + 2 + 10, y: heightOffset / 2.0 + border - 10));
        context?.addLine(to: CGPoint(x: puzzleRect.origin.x + smallImageWidth + 2 + 10, y: heightOffset / 2.0 + border + 10));
        context?.strokePath();
        
        //Draw selfie
        let selfieRect = CGRect(x: image.size.width / 2.5 + smallImageWidth + 2 + 20 + 2, y: puzzleRect.origin.y, width: smallImageWidth, height: heightOffset - yOffset);
        selfie.draw(in: selfieRect);
        
        //Draw equal
        context?.move(to: CGPoint(x: selfieRect.origin.x + smallImageWidth + 2, y: heightOffset / 2.0 - 8 + border));
        context?.addLine(to: CGPoint(x: selfieRect.origin.x + smallImageWidth + 2 + 20, y: heightOffset / 2.0 - 8 + border));
        context?.strokePath();
        context?.move(to: CGPoint(x: selfieRect.origin.x + smallImageWidth + 2, y: heightOffset / 2.0 + 4 + border));
        context?.addLine(to: CGPoint(x: selfieRect.origin.x + smallImageWidth + 2 + 20, y: heightOffset / 2.0 + 4 + border));
        context?.strokePath();
        
        //Draw result
        let resultRect = CGRect(x: selfieRect.origin.x + smallImageWidth + 2 + 20 + 2, y: puzzleRect.origin.y, width: smallImageWidth, height: heightOffset - yOffset);
        result.draw(in: resultRect);
        
        //Draw code
        let imageRect = CGRect(x: border, y: heightOffset + border + 10, width: image.size.width - 2.0 * border, height: image.size.height - 2.0 * border - 10);
        code.draw(in: imageRect);
        
        self.shareImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    //MARK: - Get share image
    func GetShare() -> UIImage
    {
        return self.shareImage!;
    }

}
