//
//  puzzle.swift
//  Jigpix
//
//  Created by Admin on 4/30/16.
//  Copyright © 2016 UNIPUZZLE, INC. All rights reserved.
//

import Foundation
import UIKit

class Puzzle {
    
    var pieces: [PuzzlePiece] = [];
    var targetPieces: [PuzzlePiece] = [];
    
    var nPiecesHoriz: Int = 0;
    var nPiecesVert: Int = 0;
    var nTotalPieces: Int = 0;
    var smallResHoriz: Int = 0;
    var smallResVert: Int = 0;
    var widthPiece: Int = 0;
    var heightPiece: Int = 0;
    
    var powerFunc: Double = 0.0;
    var centralImortance: Double = 0.0;
    
    
    //--------------------------------------------------------------
    func setup(_ cols: Int, rows: Int)
    {
        centralImortance		= 1;
        powerFunc				= 0.6;
        nPiecesHoriz			= cols;//20;
        nPiecesVert				= rows;//26;
        nTotalPieces			= nPiecesHoriz * nPiecesVert;
        smallResHoriz			= cols * CHOPSIZE; // 300;
        smallResVert			= rows * CHOPSIZE; //390;
        
        widthPiece				= smallResHoriz / nPiecesHoriz;
        heightPiece				= smallResHoriz / nPiecesHoriz;
        
        // allocate the pieces
        self.pieces = Array(repeating: PuzzlePiece(), count: nTotalPieces);
        self.targetPieces = Array(repeating: PuzzlePiece(), count: nTotalPieces);
    }
    
    //--------------------------------------------------------------
    func setFromImage(_ inputImage: UIImage, target: Bool)
    {
        let resizedImage = inputImage.resizeImage2(CGSize(width: smallResHoriz, height: smallResVert));
//        let aaa = PuzzlePiece();
//        let bbb = aaa.getPixels(resizedImage);
        let pieceSizeW = smallResHoriz / nPiecesHoriz;
        
        for p in 0..<nTotalPieces
        {
            let row = p / nPiecesHoriz;
            let col = p % nPiecesHoriz;
            
            let cropRect = CGRect(x: col * pieceSizeW, y: row * pieceSizeW, width: pieceSizeW, height: pieceSizeW);
            let cropImage = resizedImage.cropImage(cropRect);
//            let aaa = PuzzlePiece();
//            let bbb = aaa.getPixels(cropImage);
            
            if(target)
            {
                self.targetPieces[p] = PuzzlePiece();
                self.targetPieces[p].id = p;
                self.targetPieces[p].calculateStatistics(cropImage, cols: nPiecesHoriz, rows: nPiecesVert);
            }
            else
            {
                self.pieces[p] = PuzzlePiece();
                self.pieces[p].id = p;
                self.pieces[p].calculateStatistics(cropImage, cols: nPiecesHoriz, rows: nPiecesVert);
            }
        }
    }
    
    //--------------------------------------------------------------
    func tryToSwapStuff()->Bool
    {
        let randomNumber = UInt32(nTotalPieces + 1);
        let posA = Int(arc4random_uniform(randomNumber)) % nTotalPieces;
        let posB = Int(arc4random_uniform(randomNumber)) % nTotalPieces;
        
        if (posA == posB)
        {
            return false;
        }
        
        var CAatTA = 0.0, CBatTB = 0.0;
        var CAatTB = 0.0, CBatTA = 0.0;
        var AatBrots = 0, BatArots = 0, notused = 0;
        
        let TAptr = targetPieces[ posA ];
        let TBptr = targetPieces[ posB ];
        let CAptr = pieces[ posA ];
        let CBptr = pieces[ posB ];
        
        /* calc mismatches */
        CAatTA = mismatch(CAptr, chopPtr: TAptr, bestRot: &notused ); /* CCWs already recorded  */
        /* will CAatTA *= (C_CENT * TAdis); */
        
        CBatTB = mismatch(CBptr, chopPtr: TBptr, bestRot: &notused ); /* CCWs already recorded  */
        /* will CBatTB *= (C_CENT * TBdis); */
        
        CAatTB = mismatch(CAptr, chopPtr: TBptr, bestRot: &AatBrots); /* AatB, newCCWs if swapt */
        /* will CAatTB *= (C_CENT * TBdis); */
        
        CBatTA = mismatch(CBptr, chopPtr: TAptr, bestRot: &BatArots); /* BatA, newCCWs if swapt */
        /* will CBatTA += (C_CENT * TAdis); */
        
        
        //	/* Central mismatch can be 1 to 9 times as important as at corner.
        //	 * Its adjusted value f(CentralImportance * DistToCorner) is thus
        //	 * about from 100% to 900% of originglly (for max import 1 thru 9),
        //	 * approximated by: newval = oldval + (oldval*(import-1)*distance)/10
        //	 * (Note two different distances are involved for locs A & B)
        //	 */
        
        let importminus = self.centralImortance;
        
        let loca = TAptr.id;
        let locb = TBptr.id;
        
        let COLS = self.nPiecesHoriz;
        
        let Acol = loca%COLS;
        let Arow = loca/COLS;
        let Bcol = locb%COLS;
        let Brow = locb/COLS;
        
        let ax =  (Acol<10 ? Acol : 19-Acol);
        let ay =  (Arow<13 ? Arow : 25-Arow);
        let bx =  (Bcol<10 ? Bcol : 19-Bcol);
        let by =  (Brow<13 ? Brow : 25-Brow);
        
        //        let Adist = (Acol<10 ? Acol : 19-Acol)+(Arow<13 ? Arow : 25-Arow);
        //        let Bdist = (Bcol<10 ? Bcol : 19-Bcol)+(Brow<13 ? Brow : 25-Brow);
        
        var distanceA = min(pow(sqrt(Double(ax*ax)) / sqrt (10*10), powerFunc) , pow(sqrt(Double(ay*ay)) / sqrt(13*13),powerFunc));
        var distanceB = min(pow(sqrt(Double(bx*bx)) / sqrt (10*10),powerFunc) , pow(sqrt(Double(by*by)) / sqrt(13*13),powerFunc));
        
        distanceA *= (importminus / 5.0);
        distanceB *= (importminus / 5.0);
        
        CAatTA += ((CAatTA  * distanceA)) ;
        CBatTB += ((CBatTB  * distanceB)) ;
        CAatTB += ((CAatTB  * distanceB)) ;
        CBatTA += ((CBatTA  * distanceA)) ;
        
        
        //print("mismatch %i %i \n", CAatTB + CBatTA, CAatTA + CBatTB);
        /* if chopup tiles best as they are, do not swap */
        if((CAatTB + CBatTA) >= (CAatTA + CBatTB)) {
            //print("we're not swapping \n");
            return false;
        } else{
            
        }
        /* Exchange 1. plant anticipated new miss data */
        pieces[posA].rotation = AatBrots;
        pieces[posB].rotation = BatArots;
        
        let temp = pieces[posA];
        pieces[posA] = pieces[posB];
        pieces[posB] = temp;
        
        return true;
    }
    
    //--------------------------------------------------------------
    func mismatch(_ targPtr: PuzzlePiece, chopPtr: PuzzlePiece, bestRot: inout Int)-> Double
    {
        // ------------------------------------------------------------
        // find the best rotation
        // ------------------------------------------------------------
        
        var Crit = 0, Cbot = 0;  /* chopup tile */
        var Trit = 0, Tbot = 0;  /* target tile */
        var SumRitBotSq = 0.0, nowsum = 0.0;
        var newbot = 0;
        
//        Cbri = chopPtr.bri;
        Crit = chopPtr.rit;
        Cbot = chopPtr.bot;
//        Tbri = targPtr.bri;
        Trit = targPtr.rit;
        Tbot = targPtr.bot;
        
        SumRitBotSq = 999999999;   /* <-- will certainly be lowered */
        for i in 0..<4
        {
            let tmp = Double((Crit&-Trit)&*(Crit&-Trit) &+ (Cbot&-Tbot)&*(Cbot&-Tbot));
            nowsum = sqrt(tmp);
            //cout << nowsum << endl;
            if(SumRitBotSq > nowsum){
                SumRitBotSq = nowsum;
                bestRot    = i;  /* best addt'l CCWs IFF swapped */
            }
            /* rotate chopup tile vals 90 degrees (CCW TEMP VALS ONLY) */
            newbot = -Crit;
            Crit   =  Cbot;
            Cbot   =  newbot;
        }
        
        
        // ------------------------------------------------------------
        // w/ the best rotation, find the per pixel difference of the smaller images
        // based on this metric http://www.compuphase.com/cmetric.htm
        // ------------------------------------------------------------
        
        var distance = 0.0;
        
        let pixelsA = chopPtr.colorImgPixData[bestRot];
        let pixelsB = targPtr.colorImgPixData[0];
        
        for i in 0..<5
        {
            for j in 0..<5
            {
                let rmean = (pixelsA[i][j].r + pixelsB[i][j].r) / 2;
                let r = pixelsA[i][j].r - pixelsB[i][j].r;
                let g = pixelsA[i][j].g - pixelsB[i][j].g;
                let b = pixelsA[i][j].b - pixelsB[i][j].b;
                let tmp = Double((((512&+rmean)&*r&*r)>>8) &+ 4&*g&*g + (((767&-rmean)&*b&*b)>>8));
                distance += sqrt(tmp);
            }
        }
        distance /= 25.0;
        
        return(distance*distance);
        
    }
    
}
