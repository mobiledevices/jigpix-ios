//
//  UIImageExtension.swift
//  ImageFun
//
//  Created by Neeraj Kumar on 11/11/14.
//  Copyright (c) 2014 Neeraj Kumar. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    subscript (i: Int) -> Character {
        return self[self.characters.index(self.startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        return substring(with: characters.index(startIndex, offsetBy: r.lowerBound)..<characters.index(startIndex, offsetBy: r.upperBound))
    }
}

extension UIImage {
    class func createImageWithColor(_ color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size);
        let context = UIGraphicsGetCurrentContext()
        
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    func createARGBBitmapContext(_ inImage: CGImage) -> CGContext {
        
        //Get image width, height
        let pixelsWide = inImage.width
        let pixelsHigh = inImage.height
        
        // Declare the number of bytes per row. Each pixel in the bitmap in this
        // example is represented by 4 bytes; 8 bits each of red, green, blue, and
        // alpha.
        let bitmapBytesPerRow = Int(pixelsWide) * 4
        //let bitmapByteCount = bitmapBytesPerRow * Int(pixelsHigh)
        
        // Use the generic RGB color space.
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        
        // Allocate memory for image data. This is the destination in memory
        // where any drawing to the bitmap context will be rendered.
        let bitmapData: UnsafeMutablePointer<UInt8>? = nil
        // Create the bitmap context. We want pre-multiplied ARGB, 8-bits
        // per component. Regardless of what the source image format is
        // (CMYK, Grayscale, and so on) it will be converted over to the format
        // specified here by CGBitmapContextCreate.
        let context = CGContext(data: bitmapData, width: pixelsWide, height: pixelsHigh, bitsPerComponent: 8, bytesPerRow: bitmapBytesPerRow, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue)
        
        return context!
    }
    
    func sanitizePoint(_ point:CGPoint) {
        let inImage:CGImage = self.cgImage!
        let pixelsWide = inImage.width
        let pixelsHigh = inImage.height
        let rect = CGRect(x:0, y:0, width:Int(pixelsWide), height:Int(pixelsHigh))
        
        precondition(rect.contains(point), "CGPoint passed is not inside the rect of image.It will give wrong pixel and may crash.")
    }
    
    func resizeImage(_ targetSize: CGSize) -> UIImage {
        let size = self.size
        
        let widthRatio  = targetSize.width  / self.size.width
        let heightRatio = targetSize.height / self.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func resizeImage2(_ targetSize: CGSize) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: targetSize.width, height: targetSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(targetSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    public func rotate(_ angle: Int) -> UIImage {
        UIGraphicsBeginImageContext(self.size);
        let cgContext = UIGraphicsGetCurrentContext();
        
        let offset = self.size.width / 2.0
        
        cgContext?.translateBy(x: offset, y: offset)
        cgContext?.rotate(by: CGFloat(Double(angle) * Double.pi / 180))
        
        self.draw(at: CGPoint(x: -offset, y: -offset))
        
        let final = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return final!;
    }
    
    func cropImage(_ cropRect: CGRect) -> UIImage
    {
        // Draw new image in current graphics context
        let imageRef = self.cgImage?.cropping(to: cropRect);
        
        // Create new cropped UIImage
        let croppedImage = UIImage(cgImage: imageRef!);
        
        return croppedImage;
    }
    
    func imageWithColor(_ color1: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color1.setFill()
        
        let context = UIGraphicsGetCurrentContext()! as CGContext
        context.translateBy(x: 0, y: self.size.height)
        context.scaleBy(x: 1.0, y: -1.0);
        context.setBlendMode(CGBlendMode.normal)
        
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height) as CGRect
        context.clip(to: rect, mask: self.cgImage!)
        context.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()! as UIImage
        UIGraphicsEndImageContext()
        
        return newImage
    }
}


// Internal functions exposed.Can be public.

extension  UIImage {
    typealias RawColorType = (newRedColor:UInt8, newgreenColor:UInt8, newblueColor:UInt8,  newalphaValue:UInt8)
    
    /*
     Change the color of pixel at a certain point.If you want more control try block based method to modify pixels.
     */
    func setPixelColorAtPoint(_ point:CGPoint, color: RawColorType) -> UIImage? {
        self.sanitizePoint(point)
        let inImage:CGImage = self.cgImage!
        let context = self.createARGBBitmapContext(inImage)
        
        let pixelsWide = inImage.width
        let pixelsHigh = inImage.height
        let rect = CGRect(x:0, y:0, width:Int(pixelsWide), height:Int(pixelsHigh))
        
        //Clear the context
        context.clear(rect)
        
        // Draw the image to the bitmap context. Once we draw, the memory
        // allocated for the context for rendering will then contain the
        // raw image data in the specified color space.
        context.draw(inImage, in: rect)
        
        // Now we can get a pointer to the image data associated with the bitmap
        // context.
        let data = context.data!
        let dataType = data.bindMemory(to: UInt8.self, capacity: pixelsWide * pixelsHigh)
        
        let offset = 4*((Int(pixelsWide) * Int(point.y)) + Int(point.x))
        dataType[offset]   = color.newalphaValue
        dataType[offset+1] = color.newRedColor
        dataType[offset+2] = color.newgreenColor
        dataType[offset+3] = color.newblueColor
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        
        let bitmapBytesPerRow = Int(pixelsWide) * 4
        //let bitmapByteCount = bitmapBytesPerRow * Int(pixelsHigh)
        
        let finalcontext = CGContext(data: data, width: pixelsWide, height: pixelsHigh, bitsPerComponent: 8, bytesPerRow: bitmapBytesPerRow, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue)
        
        let imageRef = finalcontext?.makeImage()
        return UIImage(cgImage: imageRef!, scale: self.scale,orientation: self.imageOrientation)
    }
    
    
    /*
     Get pixel color for a pixel in the image.
     */
    func getPixelColorAtLocation(_ point:CGPoint)->UIColor? {
        self.sanitizePoint(point)
        // Create off screen bitmap context to draw the image into. Format ARGB is 4 bytes for each pixel: Alpa, Red, Green, Blue
        let inImage:CGImage = self.cgImage!
        let context = self.createARGBBitmapContext(inImage)
        
        let pixelsWide = inImage.width
        let pixelsHigh = inImage.height
        let rect = CGRect(x:0, y:0, width:Int(pixelsWide), height:Int(pixelsHigh))
        
        //Clear the context
        context.clear(rect)
        
        // Draw the image to the bitmap context. Once we draw, the memory
        // allocated for the context for rendering will then contain the
        // raw image data in the specified color space.
        context.draw(inImage, in: rect)
        
        // Now we can get a pointer to the image data associated with the bitmap
        // context.
        let data = context.data!
        let dataType = data.bindMemory(to: UInt8.self, capacity: pixelsWide * pixelsHigh)
        
        let offset = 4*((Int(pixelsWide) * Int(point.y)) + Int(point.x))
        let alphaValue = dataType[offset]
        let redColor = dataType[offset+1]
        let greenColor = dataType[offset+2]
        let blueColor = dataType[offset+3]
        
        let redFloat = CGFloat(redColor)/255.0
        let greenFloat = CGFloat(greenColor)/255.0
        let blueFloat = CGFloat(blueColor)/255.0
        let alphaFloat = CGFloat(alphaValue)/255.0
        
        return UIColor(red: redFloat, green: greenFloat, blue: blueFloat, alpha: alphaFloat)
        
        // When finished, release the context
        // Free image data memory for the context
    }
    
    
    // Get grayscale image from normal image.
    func getGrayScale() -> UIImage? {
        let inImage:CGImage = self.cgImage!
        let context = self.createARGBBitmapContext(inImage)
        let pixelsWide = inImage.width
        let pixelsHigh = inImage.height
        let rect = CGRect(x:0, y:0, width:Int(pixelsWide), height:Int(pixelsHigh))
        
        let bitmapBytesPerRow = Int(pixelsWide) * 4
        //let bitmapByteCount = bitmapBytesPerRow * Int(pixelsHigh)
        
        //Clear the context
        context.clear(rect)
        
        // Draw the image to the bitmap context. Once we draw, the memory
        // allocated for the context for rendering will then contain the
        // raw image data in the specified color space.
        context.draw(inImage, in: rect)
        
        // Now we can get a pointer to the image data associated with the bitmap
        // context.
        
        let data = context.data!
        let dataType = data.bindMemory(to: UInt8.self, capacity: pixelsWide * pixelsHigh)
        
        for x in 0..<Int(pixelsWide)
        {
            for y in 0..<Int(pixelsHigh)
            {
                let offset = 4*((Int(pixelsWide) * Int(y)) + Int(x))
                //let alpha = dataType[offset]
                let red = dataType[offset+1]
                let green = dataType[offset+2]
                let blue = dataType[offset+3]
                
                let avg = (UInt32(red) + UInt32(green) + UInt32(blue))/3
                
                dataType[offset + 1] = UInt8(avg)
                dataType[offset + 2] = UInt8(avg)
                dataType[offset + 3] = UInt8(avg)
            }
        }
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        
        let finalcontext = CGContext(data: data, width: pixelsWide, height: pixelsHigh, bitsPerComponent: 8,  bytesPerRow: bitmapBytesPerRow, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue)
        
        let imageRef = finalcontext?.makeImage()
        return UIImage(cgImage: imageRef!, scale: self.scale,orientation: self.imageOrientation)
    }
    
    
    
    // Defining the closure.
    typealias ModifyPixelsClosure = (_ point:CGPoint, _ redColor:UInt8, _ greenColor:UInt8, _ blueColor:UInt8, _ alphaValue:UInt8)->(newRedColor:UInt8, newgreenColor:UInt8, newblueColor:UInt8,  newalphaValue:UInt8)
    
    
    // Provide closure which will return new color value for pixel using any condition you want inside the closure.
    
    func applyOnPixels(_ closure:ModifyPixelsClosure) -> UIImage? {
        let inImage:CGImage = self.cgImage!
        let context = self.createARGBBitmapContext(inImage)
        let pixelsWide = inImage.width
        let pixelsHigh = inImage.height
        let rect = CGRect(x:0, y:0, width:Int(pixelsWide), height:Int(pixelsHigh))
        
        let bitmapBytesPerRow = Int(pixelsWide) * 4
        //let bitmapByteCount = bitmapBytesPerRow * Int(pixelsHigh)
        
        //Clear the context
        context.clear(rect)
        
        // Draw the image to the bitmap context. Once we draw, the memory
        // allocated for the context for rendering will then contain the
        // raw image data in the specified color space.
        context.draw(inImage, in: rect)
        
        // Now we can get a pointer to the image data associated with the bitmap
        // context.
        
        let data = context.data!
        let dataType = data.bindMemory(to: UInt8.self, capacity: pixelsWide * pixelsHigh)
        
        for x in 0..<Int(pixelsWide)
        {
            for y in 0..<Int(pixelsHigh)
            {
                let offset = 4*((Int(pixelsWide) * Int(y)) + Int(x))
                let alpha = dataType[offset]
                let red = dataType[offset+1]
                let green = dataType[offset+2]
                let blue = dataType[offset+3]
                let (newRedColor, newGreenColor, newBlueColor, newAlphaValue): (UInt8, UInt8, UInt8, UInt8)  =  closure(CGPoint(x: CGFloat(x), y: CGFloat(y)), red, green,  blue, alpha)
                dataType[offset] = newAlphaValue
                dataType[offset + 1] = newRedColor
                dataType[offset + 2] = newGreenColor
                dataType[offset + 3] = newBlueColor
            }
        }
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        
        let finalcontext = CGContext(data: data, width: pixelsWide, height: pixelsHigh, bitsPerComponent: 8,  bytesPerRow: bitmapBytesPerRow, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue)
        
        let imageRef = finalcontext?.makeImage()
        return UIImage(cgImage: imageRef!, scale: self.scale,orientation: self.imageOrientation)
    }
    
    func roundCorners() -> UIImage
    {
        // begin a new image
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale);
        //create a rectangle
        let bounds = CGRect(origin: CGPoint(x: 0, y: 0), size: self.size);
        // add a clip in the shape of a rounded rectangle
        UIBezierPath(roundedRect: bounds, cornerRadius: 10.0).addClip();
        // draw the image in the view
        self.draw(in: bounds);
        // set the image
        let image = UIGraphicsGetImageFromCurrentImageContext();
        // clean up
        UIGraphicsEndImageContext();
        
        return image!;
    }
    
    
    class func imageWithLayer(layer: CALayer) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(layer.bounds.size, layer.isOpaque, 0.0)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    public func fixedOrientation() -> UIImage {
        
        if imageOrientation == UIImageOrientation.up {
            return self
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch imageOrientation {
        case UIImageOrientation.down, UIImageOrientation.downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
            break
        case UIImageOrientation.left, UIImageOrientation.leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat(Double.pi/2.0))
            break
        case UIImageOrientation.right, UIImageOrientation.rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: CGFloat(-(Double.pi/2.0)))
            break
        case UIImageOrientation.up, UIImageOrientation.upMirrored:
            break
        }
        
        switch imageOrientation {
        case UIImageOrientation.upMirrored, UIImageOrientation.downMirrored:
            transform.translatedBy(x: size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case UIImageOrientation.leftMirrored, UIImageOrientation.rightMirrored:
            transform.translatedBy(x: size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case UIImageOrientation.up, UIImageOrientation.down, UIImageOrientation.left, UIImageOrientation.right:
            break
        }
        
        let ctx: CGContext = CGContext(data: nil,
                                       width: Int(size.width),
                                       height: Int(size.height),
                                       bitsPerComponent: self.cgImage!.bitsPerComponent,
                                       bytesPerRow: 0,
                                       space: self.cgImage!.colorSpace!,
                                       bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch imageOrientation {
        case UIImageOrientation.left, UIImageOrientation.leftMirrored, UIImageOrientation.right, UIImageOrientation.rightMirrored:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
        default:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            break
        }
        
        let cgImage: CGImage = ctx.makeImage()!
        
        return UIImage(cgImage: cgImage)
    }
}

func degreesToRadians(_ degrees:CGFloat) -> CGFloat
{
    return degrees / 180.0 * CGFloat(Double.pi)
}
