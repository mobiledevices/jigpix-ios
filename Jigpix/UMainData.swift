//
//  UMainData.swift
//  Jigpix
//
//  Created by  on 4/19/17.
//  Copyright © 2017 Unipuzzle. All rights reserved.
//

import Foundation

class UMainData
{
    static let sharedInstance = UMainData()
    
    let m_PuzzleArray = ["Venus-Birth", "08-Tiger.jpg", "Wallace-Gromit", "01-Trump", "02-Corbyn", "03-Markel", "04-Berlusconi", "05-Johnson", "06-Farage", "07-Putin", "Mona-Liza", "09-Chicken", "10-Fox", "11-Giraffe", "12-Orangutan", "13-Owl", "14-Zebra" ]
    
    var m_SelectedPuzzleRawData: Data? = nil
    
    var m_SelectedPhotoRawData: Data? = nil
    
    var m_ConvertedRawData: Data? = nil
    
    var m_CodeRawData: Data? = nil
    
    var m_ShareRawData: Data? = nil
    
    var m_IsShareImage: Bool = true
}
