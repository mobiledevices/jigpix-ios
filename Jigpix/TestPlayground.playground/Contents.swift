//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"


//func createImageForSaving() -> UIImage
//{
    let logo = UIImage(named: "jigpix")!;
    let code =  UIImage(named: "codes")!;
    let puzzle = UIImage(named: "Venus-Birth")!;
    let selfie = UIImage(named: "Mona-Liza")!;
    let result = UIImage(named: "Wallace-Gromit.jpg")!;
    
    UIGraphicsBeginImageContextWithOptions(CGSize(width: 1120.0, height: 1408.0), false, 0);
    //        self.coordinateView.drawHierarchy(in: self.coordinateView.bounds, afterScreenUpdates: true);
    let image:UIImage = UIGraphicsGetImageFromCurrentImageContext()!;
    UIGraphicsEndImageContext();
    
    let heightOffset = image.size.height / 5.0;
    let sizeResult = CGSize(width: image.size.width, height: image.size.height + heightOffset);
    let opaque = false;
    let scale: CGFloat = 0;
    UIGraphicsBeginImageContextWithOptions(sizeResult, opaque, scale);
    let context = UIGraphicsGetCurrentContext();
    
    let borderRect = CGRect(x: 0, y: 0, width: sizeResult.width, height: sizeResult.height);
    context?.setFillColor(UIColor.gray.cgColor);
    context?.addRect(borderRect);
    context?.drawPath(using: CGPathDrawingMode.fill);
    
    let border: CGFloat = 40.0;
    let mainRect = CGRect(x: border, y: border, width: sizeResult.width - 2.0 * border, height: sizeResult.height - 2.0 * border);
    context?.setFillColor(UIColor.white.cgColor);
    context?.addRect(mainRect);
    context?.drawPath(using: CGPathDrawingMode.fill);
    
    let yOffset: CGFloat = 5.0;
    //Draw logo
    let logoRect = CGRect(x: border, y: yOffset + border + 10, width: image.size.width / 2.5 - border, height: heightOffset - yOffset - 30);
    logo.draw(in: logoRect);
    
    let smallImageWidth: CGFloat = (image.size.width - image.size.width / 2.5) / 3.0 - 40;
    
    //Draw puzzle
    let puzzleRect = CGRect(x: image.size.width / 2.5, y: yOffset + border, width: smallImageWidth, height: heightOffset - yOffset);
    puzzle.draw(in: puzzleRect);
    
    context?.setLineWidth(3.0);
    context?.setStrokeColor(UIColor.red.cgColor);
    
    //Draw plus
    context?.move(to: CGPoint(x: puzzleRect.origin.x + smallImageWidth + 2, y: heightOffset / 2.0 + border));
    context?.addLine(to: CGPoint(x: puzzleRect.origin.x + smallImageWidth + 2 + 20, y: heightOffset / 2.0 + border));
    context?.strokePath();
    context?.move(to: CGPoint(x: puzzleRect.origin.x + smallImageWidth + 2 + 10, y: heightOffset / 2.0 + border - 10));
    context?.addLine(to: CGPoint(x: puzzleRect.origin.x + smallImageWidth + 2 + 10, y: heightOffset / 2.0 + border + 10));
    context?.strokePath();
    
    //Draw selfie
    let selfieRect = CGRect(x: image.size.width / 2.5 + smallImageWidth + 2 + 20 + 2, y: puzzleRect.origin.y, width: smallImageWidth, height: heightOffset - yOffset);
    selfie.draw(in: selfieRect);
    
    //Draw equal
    context?.move(to: CGPoint(x: selfieRect.origin.x + smallImageWidth + 2, y: heightOffset / 2.0 - 8 + border));
    context?.addLine(to: CGPoint(x: selfieRect.origin.x + smallImageWidth + 2 + 20, y: heightOffset / 2.0 - 8 + border));
    context?.strokePath();
    context?.move(to: CGPoint(x: selfieRect.origin.x + smallImageWidth + 2, y: heightOffset / 2.0 + 4 + border));
    context?.addLine(to: CGPoint(x: selfieRect.origin.x + smallImageWidth + 2 + 20, y: heightOffset / 2.0 + 4 + border));
    context?.strokePath();
    
    //Draw result
    let resultRect = CGRect(x: selfieRect.origin.x + smallImageWidth + 2 + 20 + 2, y: puzzleRect.origin.y, width: smallImageWidth, height: heightOffset - yOffset);
    result.draw(in: resultRect);
    
    //Draw code
    let imageRect = CGRect(x: border, y: heightOffset + border + 10, width: image.size.width - 2.0 * border, height: image.size.height - 2.0 * border - 10);
    code.draw(in: imageRect);
    
    let outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    let test = outputImage!;
//}

//let test = createImageForSaving()
