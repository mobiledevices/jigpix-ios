//
//  CustomImageFlowLayout.swift
//  Jigpix
//
//  Created by  on 4/20/17.
//  Copyright © 2017 Unipuzzle. All rights reserved.
//

import UIKit

class CustomImageFlowLayout: UICollectionViewFlowLayout {
    
    override init() {
        super.init()
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
    
    override var itemSize: CGSize {
        set {
            
        }
        get {
            let numberOfColumns: CGFloat = 2
            
            let itemWidth = (self.collectionView!.frame.width - (numberOfColumns - 1)) / numberOfColumns - 8
            let itemHeight = itemWidth + itemWidth / 3
            return CGSize(width: itemWidth, height: itemHeight)
        }
    }
    
    func setupLayout() {
        minimumInteritemSpacing = 15
        minimumLineSpacing = 15
        sectionInset.top = 15
        sectionInset.bottom = 15
        scrollDirection = .vertical
    }
    
}
