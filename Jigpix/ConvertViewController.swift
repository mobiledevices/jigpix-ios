//
//  ConvertViewController.swift
//  Jigpix
//
//  Created by  on 4/21/17.
//  Copyright © 2017 Unipuzzle. All rights reserved.
//

import UIKit

class ConvertViewController: BaseViewController, JigpixAlgoritmDelegate {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var convertButton: UIButton!
    
    fileprivate lazy var algorithm: JigpixAlgoritm? = nil;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        convertButton.addTarget(self, action: #selector(onConvertButtonTap), for: UIControlEvents.touchUpInside)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let imageData = UMainData.sharedInstance.m_SelectedPhotoRawData
        imageView.image = UIImage(data:imageData!, scale:1.0)
        
        convertButton.isEnabled = true
        convertButton.setTitle("Convert", for: UIControlState.normal)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.algorithm?.stopWorking = true;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        self.algorithm?.stopWorking = true;
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func onConvertButtonTap(sender:UIButton!) {
        //let imageName = UMainData.sharedInstance.m_PuzzleArray[UMainData.sharedInstance.m_SelectedPuzzleIndex]
        //UMainData.sharedInstance.m_ConvertedRawData = UIImagePNGRepresentation(UIImage(named: imageName)!);
        //UMainData.sharedInstance.m_CodeRawData = UIImagePNGRepresentation(UIImage(named: "codes")!);
        //self.performSegue(withIdentifier: "showResult", sender: self);
        
        startProcess();
    }
    
    //MARK: - Jigpix Algorithm
    func startProcess()
    {
        let imageData = UMainData.sharedInstance.m_SelectedPuzzleRawData
        let sourceImage = UIImage(data:imageData!, scale:1.0)!
        let targetImage = imageView.image!
        
        self.algorithm = JigpixAlgoritm()
        self.algorithm?.delegate = self
        
        let priority = DispatchQoS.QoSClass.userInitiated //the lower qos class - the slower image processing
        
        DispatchQueue.global(qos: priority).async {[unowned self] in
            self.algorithm?.Start(sourceImage, target: targetImage)
        }
    }
    
    //MARK: - Jigpix Algorithm Delegate
    func jigpixAlgoritmDidStart(_ image: UIImage) {
        imageView.image = image
        convertButton.isEnabled = false
        convertButton.setTitle("Converting...", for: UIControlState.normal)
        //scrollView.isUserInteractionEnabled = false;
    }
    
    func jigpixAlgoritmImageChanged(_ image: UIImage) {
        imageView.image = image
    }
    
    func jigpixAlgoritmDidEnd(_ image: UIImage) {
        //imageView.image = image;
        let codeImage = self.algorithm?.GetCode()
        //scrollView.isUserInteractionEnabled = true;
        UMainData.sharedInstance.m_ConvertedRawData = UIImagePNGRepresentation(image)
        UMainData.sharedInstance.m_CodeRawData = UIImagePNGRepresentation(codeImage!)
        let shareImage = self.algorithm?.GetShare()
        UMainData.sharedInstance.m_ShareRawData = UIImagePNGRepresentation(shareImage!)
        self.algorithm?.delegate = nil
        self.performSegue(withIdentifier: "showResult", sender: self)
    }
    
}
