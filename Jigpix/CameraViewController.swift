//
//  CameraViewController.swift
//  Jigpix
//
//  Created by  on 4/20/17.
//  Copyright © 2017 Unipuzzle. All rights reserved.
//

import UIKit
import AVFoundation
import PhotosUI

class CameraViewController: BaseViewController, CameraManDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var faceImageView: UIImageView!
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var switchButton: UIButton!
    
    let cameraMan = CameraMan()
    let configuration = Configuration()
    
    var previewLayer: AVCaptureVideoPreviewLayer?
    
    var isFlashOn = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        flashButton.addTarget(self, action: #selector(onFlashButtonTap), for: UIControlEvents.touchUpInside)
        cameraButton.addTarget(self, action: #selector(onCameraButtonTap), for: UIControlEvents.touchUpInside)
        switchButton.addTarget(self, action: #selector(onSwitchButtonTap), for: UIControlEvents.touchUpInside)
        
        cameraMan.delegate = self
        cameraMan.setup(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        previewLayer?.connection.videoOrientation = .portrait
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            // do stuff 0.5 seconds later
            self.faceImageView.isHidden = false;
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        faceImageView.isHidden = true;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: - Camera actions
    
    func onFlashButtonTap(sender:UIButton!) {
        isFlashOn = !isFlashOn
        flashCamera(isFlashOn ? "ON" : "OFF")
        
        //        let flashMode = AVCaptureFlashMode.on;
        //
        //        cameraMan.currentInput?.device.isFlashModeSupported(AVCaptureFlashMode.on)
        //
        //        func flash(_ mode: AVCaptureFlashMode) {
        //            guard let device = currentInput?.device, device.isFlashModeSupported(mode) else { return }
        //
        //            queue.async {
        //                self.lock {
        //                    device.flashMode = mode
        //                }
        //            }
        //        }
    }
    
    func flashCamera(_ title: String) {
        let mapping: [String: AVCaptureFlashMode] = [
            "ON": .on,
            "OFF": .off
        ]
        
        cameraMan.flash(mapping[title] ?? .auto)
    }
    
    func onCameraButtonTap(sender:UIButton!) {
        //let image = self.imageWithView(view: imageView)
        //        let image = UIImage.imageWithLayer(layer: previewLayer!)
        //        UMainData.sharedInstance.m_SelectedPhotoRawData = UIImagePNGRepresentation(image)
        //        self.performSegue(withIdentifier: "showConvertFromCamera", sender: self)
        
        cameraMan.stillImageOutput?.captureStillImageAsynchronously(from: cameraMan.stillImageOutput?.connection(withMediaType: AVMediaTypeVideo))
        { buffer, error in
            let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(buffer)
            if(imageData != nil)
            {
                var image = UIImage(data: imageData!)
                if(USettingsData.sharedInstance.SaveOriginalPhotos)
                {
                    UIImageWriteToSavedPhotosAlbum(image!, self, nil, nil)
                }
                image = image?.fixedOrientation();
                image = image?.resizeImage2(self.view.bounds.size)
                let cropRect = CGRect(x: self.imageView.frame.origin.x, y: self.imageView.frame.origin.y, width: self.imageView.frame.width, height: self.imageView.frame.height)
                image = image?.cropImage(cropRect)
                UMainData.sharedInstance.m_SelectedPhotoRawData = UIImagePNGRepresentation(image!)
                self.performSegue(withIdentifier: "showConvertFromCamera", sender: self)
            }
        }
        
        //self.takePicture()
    }
    
    //    func imageFromLayer(layer:CALayer)->UIImage?
    //    {
    //        UIGraphicsBeginImageContextWithOptions(layer.bounds.size, layer.isOpaque, 0);
    //        let context = UIGraphicsGetCurrentContext()!
    //        layer.render(in: context);
    //        let outputImage = UIGraphicsGetImageFromCurrentImageContext();
    //        UIGraphicsEndImageContext();
    //
    //        return outputImage;
    //    }
    //
    //    func imageWithView(view:UIView) -> UIImage? {
    //        UIGraphicsBeginImageContextWithOptions(view.bounds.size, true, 0)
    //        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
    //        let image = UIGraphicsGetImageFromCurrentImageContext()
    //        UIGraphicsEndImageContext()
    //        return image
    //    }
    
    func onSwitchButtonTap(sender:UIButton!) {
        UIView.animate(withDuration: 0.3, animations: { _ in
            self.imageView.alpha = 0
        }, completion: { _ in
            self.cameraMan.switchCamera {
                UIView.animate(withDuration: 0.7, animations: {
                    self.imageView.alpha = 1
                })
            }
        })
    }
    
    func takePicture() {
        guard let previewLayer = previewLayer else { return }
        
        UIView.animate(withDuration: 0.1, animations: {
            self.imageView.alpha = 1
        }, completion: { _ in
            UIView.animate(withDuration: 0.1, animations: {
                self.imageView.alpha = 0
            })
        })
        
        cameraMan.takePhoto(previewLayer)
        {
            image in
            if(image != nil)
            {
                UMainData.sharedInstance.m_SelectedPhotoRawData = UIImagePNGRepresentation(image!)
                self.performSegue(withIdentifier: "showConvertFromCamera", sender: self)
            }
        }
    }
    
    // CameraManDelegate
    func cameraManNotAvailable(_ cameraMan: CameraMan) {
        //        showNoCamera(true)
        //        focusImageView.isHidden = true
        //        delegate?.cameraNotAvailable()
    }
    
    func cameraMan(_ cameraMan: CameraMan, didChangeInput input: AVCaptureDeviceInput) {
        //delegate?.setFlashButtonHidden(!input.device.hasFlash)
    }
    
    func cameraManDidStart(_ cameraMan: CameraMan) {
        setupPreviewLayer()
    }
    
    func setupPreviewLayer() {
        guard let layer = AVCaptureVideoPreviewLayer(session: cameraMan.session) else { return }
        
        layer.backgroundColor = configuration.mainColor.cgColor
        layer.autoreverses = true
        layer.videoGravity = AVLayerVideoGravityResizeAspectFill
        
        imageView.layer.insertSublayer(layer, at: 0)
        layer.frame = imageView.bounds //CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: imageView.layer.frame.width, height: imageView.layer.frame.height)
        imageView.clipsToBounds = true
        
        previewLayer = layer
    }
}
