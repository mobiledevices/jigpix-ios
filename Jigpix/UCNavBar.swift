//
//  UCNavBar.swift
//  Jigpix
//
//  Created by  on 4/17/17.
//  Copyright © 2017 Unipuzzle. All rights reserved.
//

import UIKit

//MARK: - Protocol UCNavBarDelegate
protocol UCNavBarDelegate {
    func UTapOnHeaderImage(_ imageView: UIImageView)
}

// Create UCNavBar, set File's Owner to UCNavBar.
// Link the top level view in the XIB to the contentView outlet.
class UCNavBar : UIView {
    @IBOutlet private var contentView:UIView?
    // other outlets
    @IBOutlet private var titleLabel:UILabel?
    @IBOutlet var backButton:UIButton?
    @IBOutlet var settingsButton:UIButton?
    @IBOutlet var titleImageView:UIImageView?
    
    private var isBackVisible = true;
    
    var delegate: UCNavBarDelegate? = nil;
    
    var IsBackVisible: Bool {
        get {
            return (backButton?.isHidden)!
        }
        set(newIsBackVisible) {
            backButton?.isHidden = !newIsBackVisible
        }
    }
    
    override init(frame: CGRect) { // for using CustomView in code
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) { // for using CustomView in IB
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("UCNavBar", owner: self, options: nil)
        guard let content = contentView else { return }
        content.frame = self.bounds
        content.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(content)
        
        self.titleLabel?.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 80)
        
        self.backButton?.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(secretTapOnHeaderImage))
        tap.numberOfTapsRequired = 3
        self.titleImageView?.addGestureRecognizer(tap)
    }
    
    func secretTapOnHeaderImage(sender: UITapGestureRecognizer)
    {
        self.delegate?.UTapOnHeaderImage(titleImageView!);
    }
}
