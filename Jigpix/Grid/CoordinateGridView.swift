//
//  CoordinateGridView.swift
//  Jigpix
//
//  Created by  on 4/19/17.
//  Copyright © 2017 Unipuzzle. All rights reserved.
//

import UIKit

class CoordinateGridView: UIView {
    
    fileprivate var _columns: Int = 0;
    fileprivate var _rows: Int = 0;
    
    fileprivate var _zoomScale: CGFloat = 1.0;
    fileprivate var _leftContentOffset: CGFloat = 0.0;
    fileprivate var _topContentOffset: CGFloat = 0.0;
    
    fileprivate let cStrArray: NSString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    
    fileprivate var _showGrid = true;
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        
        let context = UIGraphicsGetCurrentContext()
        
        if(self._showGrid)
        {
            //self.backgroundColor = UIColor.clearColor();
            
            let imageScrollView = self.subviews[0];
            
            let h = imageScrollView.frame.height / CGFloat(self._rows);
            let w = imageScrollView.frame.width / CGFloat(self._columns);
            
            let hOff = (self.frame.height - imageScrollView.frame.height) / 2;
            let wOff = (self.frame.width - imageScrollView.frame.width) / 2;
            
            let font = UIFont.systemFont(ofSize: 12);
            
            let parStyle = NSMutableParagraphStyle()
            parStyle.alignment = NSTextAlignment.center;
            
            let textAttributes: [String: AnyObject] = [
                NSFontAttributeName : font,
                NSParagraphStyleAttributeName: parStyle,
                NSForegroundColorAttributeName : UIColor.black
            ]
            
            let strokeColor = UIColor.clear.cgColor;
            
            //Draw Columns
            for col in 0..<self._columns
            {
                let width = w * self._zoomScale;
                
                //Top
                context?.setLineWidth(2.0)
                context?.setStrokeColor(strokeColor)
                var rectangleT = CGRect(x: wOff + width*CGFloat(col) - self._leftContentOffset, y: 0, width: width, height: hOff)
                context?.addRect(rectangleT)
                context?.strokePath()
                
                //Bottom
                context?.setLineWidth(2.0)
                context?.setStrokeColor(strokeColor)
                var rectangleB = CGRect(x: wOff + width*CGFloat(col) - self._leftContentOffset, y: self.frame.height - hOff, width: width, height: hOff)
                context?.addRect(rectangleB)
                context?.strokePath()
                let text: NSString = cStrArray.substring(with: NSRange(location: col, length: 1)) as NSString;
                rectangleT.origin.y = rectangleT.origin.y + ((rectangleT.size.height - font.pointSize) / 2.0);
                rectangleB.origin.y = rectangleB.origin.y + ((rectangleB.size.height - font.pointSize) / 2.0);
                
                text.draw(in: rectangleT, withAttributes: textAttributes)
                text.draw(in: rectangleB, withAttributes: textAttributes)
            }
            
            //Draw Rows
            for row in 0..<self._rows
            {
                let height = h * self._zoomScale;
                
                //Left
                context?.setLineWidth(2.0)
                context?.setStrokeColor(strokeColor)
                var rectangleL = CGRect(x: 0, y: hOff + height*CGFloat(row) - self._topContentOffset, width: wOff, height: height)
                context?.addRect(rectangleL)
                context?.strokePath()
                
                //Right
                context?.setLineWidth(2.0)
                context?.setStrokeColor(strokeColor)
                var rectangleR = CGRect(x: self.frame.width - wOff, y: hOff + height*CGFloat(row) - self._topContentOffset, width: wOff, height: height)
                context?.addRect(rectangleR)
                context?.strokePath()
                
                rectangleL.origin.y = rectangleL.origin.y + ((rectangleL.size.height - font.pointSize) / 2.0);
                rectangleR.origin.y = rectangleR.origin.y + ((rectangleR.size.height - font.pointSize) / 2.0);
                let text: NSString = NSString(format: "%d", row + 1);
                text.draw(in: rectangleL, withAttributes: textAttributes)
                text.draw(in: rectangleR, withAttributes: textAttributes)
            }
            
            let fillColor = UIColor.white.cgColor;
            
            //Top Left
            context?.setLineWidth(2.0)
            context?.setFillColor(fillColor)
            let rectangleTL = CGRect(x: 0, y: 0, width: wOff, height: hOff)
            context?.addRect(rectangleTL)
            context?.fillPath()
            
            //Top Right
            context?.setLineWidth(2.0)
            context?.setFillColor(fillColor)
            let rectangleTR = CGRect(x: self.frame.width - wOff, y: 0, width: wOff, height: hOff)
            context?.addRect(rectangleTR)
            context?.fillPath()
            
            //Bottom Left
            context?.setLineWidth(2.0)
            context?.setFillColor(fillColor)
            let rectangleBL = CGRect(x: 0, y: self.frame.height - hOff, width: wOff, height: hOff)
            context?.addRect(rectangleBL)
            context?.fillPath()
            
            //Bottom Right
            context?.setLineWidth(2.0)
            context?.setFillColor(fillColor)
            let rectangleBR = CGRect(x: self.frame.width - wOff, y: self.frame.height - hOff, width: wOff, height: hOff)
            context?.addRect(rectangleBR)
            context?.fillPath()
        }
        else
        {
            context?.setFillColor(UIColor.white.cgColor)
            context?.addRect(rect)
            context?.fillPath()
        }
    }
    
    func ShowGrid(_ showGrid: Bool, columns: Int, rows: Int)
    {
        self._columns = columns;
        self._rows = rows;
        
        self._showGrid = showGrid;
        
        if(self._showGrid)
        {
            self._zoomScale = 1.0;
            self._leftContentOffset = 0;
            self._topContentOffset = 0;
        }
        
        self.setNeedsDisplay();
    }
    
    func onScrollChanged(_ zoomScale: CGFloat, leftContentOffset: CGFloat, topContentOffset: CGFloat)
    {
        self._zoomScale = zoomScale;
        self._leftContentOffset = leftContentOffset;
        self._topContentOffset = topContentOffset;
        
        self.setNeedsDisplay();
    }
}
