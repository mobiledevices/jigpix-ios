//
//  USettingsData.swift
//  Jigpix
//
//  Created by  on 4/26/17.
//  Copyright © 2017 Unipuzzle. All rights reserved.
//

import Foundation

class USettingsData
{
    static let sharedInstance = USettingsData()
    
    var SaveOriginalPhotos: Bool {
        get {
            if(isKeyPresentInUserDefaults(key: "SaveOriginalPhotos"))
            {
                return UserDefaults.standard.bool(forKey: "SaveOriginalPhotos")
            }
            
            return true
        }
        set(newSaveOriginalPhotos) {
            UserDefaults.standard.set(newSaveOriginalPhotos, forKey: "SaveOriginalPhotos")
        }
    }
    
    var SavePuzzleArtwork: Bool {
        get {
            if(isKeyPresentInUserDefaults(key: "SavePuzzleArtwork"))
            {
                return UserDefaults.standard.bool(forKey: "SavePuzzleArtwork")
            }
            
            return false
        }
        set(newSavePuzzleArtwork) {
            UserDefaults.standard.set(newSavePuzzleArtwork, forKey: "SavePuzzleArtwork")
        }
    }
    
    var CentralImportance: Double {
        get {
            if(isKeyPresentInUserDefaults(key: "CentralImportance"))
            {
                return UserDefaults.standard.double(forKey: "CentralImportance")
            }
            
            return 60.0
        }
        set(newCentralImportance) {
            UserDefaults.standard.set(newCentralImportance, forKey: "CentralImportance")
        }
    }
    
    var PowerFunction: Double {
        get {
            if(isKeyPresentInUserDefaults(key: "PowerFunction"))
            {
                return UserDefaults.standard.double(forKey: "PowerFunction")
            }
            
            return 3.0
        }
        set(newPowerFunction) {
            UserDefaults.standard.set(newPowerFunction, forKey: "PowerFunction")
        }
    }
    
    var RefreshRate: Int {
        get {
            if(isKeyPresentInUserDefaults(key: "RefreshRate"))
            {
                return UserDefaults.standard.integer(forKey: "RefreshRate")
            }
            
            return 10000
        }
        set(newRefreshRate) {
            UserDefaults.standard.set(newRefreshRate, forKey: "RefreshRate")
        }
    }
    
    var MainCycle: Int {
        get {
            if(isKeyPresentInUserDefaults(key: "MainCycle"))
            {
                return UserDefaults.standard.integer(forKey: "MainCycle")
            }
            
            return 1300000
        }
        set(newMainCycle) {
            UserDefaults.standard.set(newMainCycle, forKey: "MainCycle")
        }
    }
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    func setSecretSettingsDefaults()
    {
        if(isKeyPresentInUserDefaults(key: "CentralImportance"))
        {
            UserDefaults.standard.removeObject(forKey: "CentralImportance")
        }
        if(isKeyPresentInUserDefaults(key: "PowerFunction"))
        {
            UserDefaults.standard.removeObject(forKey: "PowerFunction")
        }
        if(isKeyPresentInUserDefaults(key: "RefreshRate"))
        {
            UserDefaults.standard.removeObject(forKey: "RefreshRate")
        }
        if(isKeyPresentInUserDefaults(key: "MainCycle"))
        {
            UserDefaults.standard.removeObject(forKey: "MainCycle")
        }
    }
}
