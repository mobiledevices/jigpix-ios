//
//  StartViewController.swift
//  Jigpix
//
//  Created by  on 4/20/17.
//  Copyright © 2017 Unipuzzle. All rights reserved.
//

import UIKit
import Photos

class StartViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pickYourPuzzleLabel: UILabel!
    
    var collectionViewLayout: CustomImageFlowLayout!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        navBarView?.IsBackVisible = false
        
        collectionViewLayout = CustomImageFlowLayout()
        collectionView.collectionViewLayout = collectionViewLayout
        collectionView.backgroundColor = .white
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(secretTapOnHeader))
        tap.numberOfTapsRequired = 3
        pickYourPuzzleLabel.addGestureRecognizer(tap)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return UMainData.sharedInstance.m_PuzzleArray.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ImageCollectionViewCell
        
        let imageName = UMainData.sharedInstance.m_PuzzleArray[indexPath.row]
        cell.imageView.image = UIImage(named: imageName)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let cell = collectionView.cellForItem(at: indexPath) as! ImageCollectionViewCell
        let image = cell.imageView.image
        UMainData.sharedInstance.m_SelectedPuzzleRawData = UIImagePNGRepresentation(image!)
        self.performSegue(withIdentifier: "showYourPuzzle", sender: self)
    }
    
    func secretTapOnHeader(sender: UITapGestureRecognizer)
    {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            self.openPhoto();
            break;
        case .denied, .restricted :
            self.alertToEncourageAccess("Library");
            break;
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization() { (status) -> Void in
                if status == .authorized
                {
                    self.openPhoto();
                }
            }
        }
        
    }
    
    fileprivate func openPhoto()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
        {
            let imagePicker = UIImagePickerController();
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = false;
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func alertToEncourageAccess(_ text: String)
    {
        //Camera not available - Alert
        let cameraUnavailableAlertController = UIAlertController (title: "\(text) Unavailable", message: "Please check to see if it is disconnected or in use by another application", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .destructive) { (_) -> Void in
            let settingsUrl = URL(string:UIApplicationOpenSettingsURLString)
            if let url = settingsUrl {
                DispatchQueue.main.async {
                    UIApplication.shared.openURL(url)
                }
                
            }
        }
        let cancelAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
        cameraUnavailableAlertController .addAction(settingsAction)
        cameraUnavailableAlertController .addAction(cancelAction)
        self.present(cameraUnavailableAlertController , animated: true, completion: nil)
    }
    
    //MARK: UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var image = info[UIImagePickerControllerOriginalImage] as! UIImage
        image = image.fixedOrientation();
        UMainData.sharedInstance.m_SelectedPuzzleRawData = UIImagePNGRepresentation(image)
        
        // Dismiss the picker.
        dismiss(animated: true, completion: nil)
        
        self.performSegue(withIdentifier: "showYourPuzzle", sender: self)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        //self.needRefresh = false;
        self.dismiss(animated: true, completion: nil);
    }
    
}
