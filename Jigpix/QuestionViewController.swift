//
//  QuestionViewController.swift
//  Jigpix
//
//  Created by  on 4/20/17.
//  Copyright © 2017 Unipuzzle. All rights reserved.
//

import UIKit

class QuestionViewController: BaseViewController {

    @IBOutlet var backButton:UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.backButton?.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        self.backButton?.addTarget(self, action: #selector(onBackButtonTap), for: UIControlEvents.touchUpInside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
