//
//  ResultViewController.swift
//  Jigpix
//
//  Created by  on 4/21/17.
//  Copyright © 2017 Unipuzzle. All rights reserved.
//

import UIKit

class ResultViewController: BaseViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var originalButton: UIButton!
    @IBOutlet weak var swapButton: UIButton!
    @IBOutlet weak var puzzleButton: UIButton!
    
    var isOriginal = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        originalButton.addTarget(self, action: #selector(onOriginalButtonTap), for: UIControlEvents.touchUpInside)
        puzzleButton.addTarget(self, action: #selector(onPuzzleButtonTap), for: UIControlEvents.touchUpInside)
        swapButton.addTarget(self, action: #selector(onSwapButtonTap), for: UIControlEvents.touchUpInside)
        
        let imageData = UMainData.sharedInstance.m_ConvertedRawData
        imageView.image = UIImage(data:imageData!, scale:1.0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func onOriginalButtonTap(sender:UIButton!) {
        if(!isOriginal)
        {
            swapButton.sendActions(for: UIControlEvents.touchUpInside)
        }
    }
    
    func onPuzzleButtonTap(sender:UIButton!) {
        if(isOriginal)
        {
            swapButton.sendActions(for: UIControlEvents.touchUpInside)
        }
    }
    
    func onSwapButtonTap(sender:UIButton!) {
        isOriginal = !isOriginal
        self.setImage()
    }
    
    func setImage()
    {
        var imageData:Data? = nil
        
        if(isOriginal)
        {
            imageData = UMainData.sharedInstance.m_SelectedPhotoRawData
        }
        else
        {
            imageData = UMainData.sharedInstance.m_ConvertedRawData
        }
        
        imageView.image = UIImage(data:imageData!, scale:1.0)
        
        let originalButtonBC = originalButton.backgroundColor
        let originalButtonTC = originalButton.titleColor(for: UIControlState.normal)
        
        let puzzleButtonBC = puzzleButton.backgroundColor
        let puzzleButtonTC = puzzleButton.titleColor(for: UIControlState.normal)
        
        puzzleButton.backgroundColor = originalButtonBC
        puzzleButton.setTitleColor(originalButtonTC, for: UIControlState.normal)
        
        originalButton.backgroundColor = puzzleButtonBC
        originalButton.setTitleColor(puzzleButtonTC, for: UIControlState.normal)
    }
    
}
