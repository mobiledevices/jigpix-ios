//
//  CodeViewController.swift
//  Jigpix
//
//  Created by  on 4/21/17.
//  Copyright © 2017 Unipuzzle. All rights reserved.
//

import UIKit

class CodeViewController: BaseViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var puzzleButton: UIButton!
    @IBOutlet weak var swapButton: UIButton!
    @IBOutlet weak var codeButton: UIButton!
    @IBOutlet weak var coordinateGridView: CoordinateGridView?
    
    var isPuzzle = false;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        codeButton.addTarget(self, action: #selector(onCodeButtonTap), for: UIControlEvents.touchUpInside)
        puzzleButton.addTarget(self, action: #selector(onPuzzleButtonTap), for: UIControlEvents.touchUpInside)
        swapButton.addTarget(self, action: #selector(onSwapButtonTap), for: UIControlEvents.touchUpInside)
        
        let imageData = UMainData.sharedInstance.m_CodeRawData
        imageView.image = UIImage(data:imageData!, scale:1.0)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        coordinateGridView?.ShowGrid(true, columns: JigpixAlgoritm.COLS, rows: JigpixAlgoritm.ROWS)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func onCodeButtonTap(sender:UIButton!) {
        if(isPuzzle)
        {
            swapButton.sendActions(for: UIControlEvents.touchUpInside)
        }
    }
    
    func onPuzzleButtonTap(sender:UIButton!) {
        if(!isPuzzle)
        {
            swapButton.sendActions(for: UIControlEvents.touchUpInside)
        }
    }
    
    func onSwapButtonTap(sender:UIButton!) {
        isPuzzle = !isPuzzle
        UMainData.sharedInstance.m_IsShareImage = !isPuzzle
        self.setImage()
    }
    
    func setImage()
    {
        var imageData:Data? = nil
        
        if(isPuzzle)
        {
            imageData = UMainData.sharedInstance.m_ConvertedRawData
        }
        else
        {
            imageData = UMainData.sharedInstance.m_CodeRawData
        }
        
        imageView.image = UIImage(data:imageData!, scale:1.0)
        
        let originalButtonBC = codeButton.backgroundColor
        let originalButtonTC = codeButton.titleColor(for: UIControlState.normal)
        
        let puzzleButtonBC = puzzleButton.backgroundColor
        let puzzleButtonTC = puzzleButton.titleColor(for: UIControlState.normal)
        
        puzzleButton.backgroundColor = originalButtonBC
        puzzleButton.setTitleColor(originalButtonTC, for: UIControlState.normal)
        
        codeButton.backgroundColor = puzzleButtonBC
        codeButton.setTitleColor(puzzleButtonTC, for: UIControlState.normal)
    }
    
}

