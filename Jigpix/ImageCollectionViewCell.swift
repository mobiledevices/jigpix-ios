//
//  ImageCollectionViewCell.swift
//  Jigpix
//
//  Created by  on 4/20/17.
//  Copyright © 2017 Unipuzzle. All rights reserved.
//
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
    }
}

