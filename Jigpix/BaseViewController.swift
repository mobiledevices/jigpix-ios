//
//  BaseViewController.swift
//  Jigpix
//
//  Created by  on 4/19/17.
//  Copyright © 2017 Unipuzzle. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, UCNavBarDelegate {
    
    @IBOutlet var navBarView: UCNavBar?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        navBarView?.backButton?.addTarget(self, action: #selector(onBackButtonTap), for: UIControlEvents.touchUpInside)
        navBarView?.settingsButton?.addTarget(self, action: #selector(onSettingsButtonTap), for: UIControlEvents.touchUpInside)
        navBarView?.delegate = self
        
        //SideMenu Setup
        // Define the menus
        //SideMenuManager.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        SideMenuManager.menuRightNavigationController = storyboard!.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? UISideMenuNavigationController
        
        if(navBarView != nil)
        {
            // Enable gestures. The left and/or right menus must be set up above for these to work.
            // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
            SideMenuManager.menuAddPanGestureToPresent(toView: navBarView!)
            //SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
            
            SideMenuManager.menuWidth = max(round(min((SideMenuManager.appScreenRect.width), (SideMenuManager.appScreenRect.height)) * 0.85), 240)
            
            //NotificationCenter.default.addObserver(self, selector: #selector(willResignActive), name: .UIApplicationWillResignActive, object: nil)
        }
    }
    
//    func willResignActive(_ notification: Notification) {
//        // code to execute
//        navBarView?.settingsButton?.sendActions(for: UIControlEvents.touchUpInside)
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func onBackButtonTap(sender:UIButton!) {
        navigationController?.popViewController(animated: true)
        //pop dismiss(animated: true, completion: nil)
    }
    
    func onSettingsButtonTap(sender:UIButton!) {
        navigationController?.performSegue(withIdentifier: "RightMenuNavigationController", sender: self)
    }
    
    func UTapOnHeaderImage(_ imageView: UIImageView)
    {
        navigationController?.performSegue(withIdentifier: "showSecretSettingsMenu", sender: self)
    }
}
