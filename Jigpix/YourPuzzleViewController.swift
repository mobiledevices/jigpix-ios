//
//  YourPuzzleViewController.swift
//  Jigpix
//
//  Created by  on 4/20/17.
//  Copyright © 2017 Unipuzzle. All rights reserved.
//

import UIKit

class YourPuzzleViewController: BaseViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let imageData = UMainData.sharedInstance.m_SelectedPuzzleRawData
        imageView.image = UIImage(data:imageData!, scale:1.0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
