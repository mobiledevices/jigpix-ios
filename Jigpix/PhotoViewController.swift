//
//  PhotoViewController.swift
//  Jigpix
//
//  Created by  on 4/20/17.
//  Copyright © 2017 Unipuzzle. All rights reserved.
//

import UIKit
import Photos

class PhotoViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var faceImageView: UIImageView!
    @IBOutlet weak var continueButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        continueButton.addTarget(self, action: #selector(onContinueButtonTap), for: UIControlEvents.touchUpInside)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            // do stuff 0.5 seconds later
            self.faceImageView.isHidden = false;
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        faceImageView.isHidden = true;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func onContinueButtonTap(sender:UIButton!) {
        if(imageView.image != nil)
        {
            UMainData.sharedInstance.m_SelectedPhotoRawData = UIImagePNGRepresentation(imageView.image!);
            self.performSegue(withIdentifier: "showConvertFromPhoto", sender: self)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return UMainData.sharedInstance.m_PuzzleArray.count + 1;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ImageCollectionViewCell
        
        if(indexPath.row == 0)
        {
            cell.imageView.image = UIImage(named: "plus")
        }
        else
        {
            let imageName = UMainData.sharedInstance.m_PuzzleArray[indexPath.row - 1]
            cell.imageView.image = UIImage(named: imageName)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if(indexPath.row == 0)
        {
            let status = PHPhotoLibrary.authorizationStatus()
            switch status {
            case .authorized:
                self.openPhoto();
                break;
            case .denied, .restricted :
                self.alertToEncourageAccess("Library");
                break;
            case .notDetermined:
                PHPhotoLibrary.requestAuthorization() { (status) -> Void in
                    if status == .authorized
                    {
                        self.openPhoto();
                    }
                }
            }
            
        }
        else
        {
            let imageName = UMainData.sharedInstance.m_PuzzleArray[indexPath.row - 1]
            imageView.image = UIImage(named: imageName)
        }
    }
    
    fileprivate func openPhoto()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
        {
            let imagePicker = UIImagePickerController();
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
            imagePicker.allowsEditing = false;
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func alertToEncourageAccess(_ text: String)
    {
        //Camera not available - Alert
        let cameraUnavailableAlertController = UIAlertController (title: "\(text) Unavailable", message: "Please check to see if it is disconnected or in use by another application", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .destructive) { (_) -> Void in
            let settingsUrl = URL(string:UIApplicationOpenSettingsURLString)
            if let url = settingsUrl {
                DispatchQueue.main.async {
                    UIApplication.shared.openURL(url)
                }
                
            }
        }
        let cancelAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
        cameraUnavailableAlertController .addAction(settingsAction)
        cameraUnavailableAlertController .addAction(cancelAction)
        self.present(cameraUnavailableAlertController , animated: true, completion: nil)
    }
    
    //MARK: UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var image = info[UIImagePickerControllerOriginalImage] as! UIImage
        image = image.fixedOrientation();
//        let cropSize = CGSize(width: self.view.bounds.width, height: self.view.bounds.height + CGFloat(77.0))
//        image = image.resizeImage2(cropSize)
//        let cropRect = CGRect(x: self.imageView.frame.origin.x, y: self.imageView.frame.origin.y, width: self.imageView.frame.width, height: self.imageView.frame.height + CGFloat(77.0))
//        image = image.cropImage(cropRect)
        imageView.image = image
        
        // Dismiss the picker.
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        //self.needRefresh = false;
        self.dismiss(animated: true, completion: nil);
    }
}
