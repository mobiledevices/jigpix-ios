//
//  UCTabBar.swift
//  Jigpix
//
//  Created by  on 4/22/17.
//  Copyright © 2017 Unipuzzle. All rights reserved.
//

import UIKit
import Social

class UCTabBar: UIView {
    @IBOutlet private var contentView:UIView?
    // other outlets
    @IBOutlet private var instagramButton:UIButton?
    @IBOutlet private var facebookButton:UIButton?
    @IBOutlet private var saveButton:UIButton?
    @IBOutlet private var shareButton:UIButton?
    
    override init(frame: CGRect) { // for using CustomView in code
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) { // for using CustomView in IB
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("UCTabBar", owner: self, options: nil)
        guard let content = contentView else { return }
        content.frame = self.bounds
        content.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(content)
        
        //self.backButton?.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        
        instagramButton?.addTarget(self, action: #selector(onInstagramButtonTap), for: UIControlEvents.touchUpInside)
        facebookButton?.addTarget(self, action: #selector(onFacebookButtonTap), for: UIControlEvents.touchUpInside)
        saveButton?.addTarget(self, action: #selector(onSaveButtonTap), for: UIControlEvents.touchUpInside)
        shareButton?.addTarget(self, action: #selector(onShareButtonTap), for: UIControlEvents.touchUpInside)
    }
    
    func onInstagramButtonTap(sender:UIButton!) {
        let image = createImageForShare()
        InstagramManager.sharedManager.postImageToInstagramWithCaption(image, instagramCaption: "Send from Jigpix", view: self);
    }
    
    func onFacebookButtonTap(sender:UIButton!) {
        let image = createImageForShare()
        
        let composeSheet = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
        composeSheet?.setInitialText("Send from Jigpix")
        composeSheet?.add(image)
        
        getCurrentViewController()?.present(composeSheet!, animated: true, completion: nil)
    }
    
    func onSaveButtonTap(sender:UIButton!) {
        let image = createImageForShare()
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer)
    {
        if error == nil {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            getCurrentViewController()?.present(ac, animated: true, completion: nil)
        } else {
            let ac = UIAlertController(title: "Save error", message: error?.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            getCurrentViewController()?.present(ac, animated: true, completion: nil)
        }
    }
    
    func onShareButtonTap(sender:UIButton!) {
        let image = createImageForShare()
        let imageToShare = [ image ];
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.postToFacebook]
        activityViewController.popoverPresentationController?.sourceView = sender
        getCurrentViewController()?.present(activityViewController, animated: true, completion: {})
    }
    
    func getCurrentViewController() -> UIViewController? {
        
        if let rootController = UIApplication.shared.keyWindow?.rootViewController {
            var currentController: UIViewController! = rootController
            while( currentController.presentedViewController != nil ) {
                currentController = currentController.presentedViewController
            }
            return currentController
        }
        return nil
        
    }
    
    func createImageForShare() -> UIImage {
        let imageData = UMainData.sharedInstance.m_IsShareImage ? UMainData.sharedInstance.m_ShareRawData : UMainData.sharedInstance.m_ConvertedRawData
        return UIImage(data:imageData!, scale:1.0)!
    }
    
}
