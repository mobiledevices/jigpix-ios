//
//  SettingsTableViewController.swift
//  Jigpix
//
//  Created by  on 4/22/17.
//  Copyright © 2017 Unipuzzle. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {
    
    @IBOutlet weak var saveOriginalPhotosSwitch: UISwitch!
    @IBOutlet weak var savePuzzleArtworkSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        saveOriginalPhotosSwitch.addTarget(self, action: #selector(saveOriginalPhotosSwitchChanged), for: UIControlEvents.valueChanged)
        savePuzzleArtworkSwitch.addTarget(self, action: #selector(savePuzzleArtworkSwitchChanged), for: UIControlEvents.valueChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        saveOriginalPhotosSwitch.isOn = USettingsData.sharedInstance.SaveOriginalPhotos
        savePuzzleArtworkSwitch.isOn = USettingsData.sharedInstance.SavePuzzleArtwork
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    /*
     override func numberOfSections(in tableView: UITableView) -> Int {
     // #warning Incomplete implementation, return the number of sections
     return 0
     }
     
     override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     // #warning Incomplete implementation, return the number of rows
     return 0
     }*/
    
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let rowCount = CGFloat(7.0) //Default cell size.
        let firstRowHeight = CGFloat(80.0)
        let defaultRowHeight = CGFloat(44.0)
        
        if(indexPath.row == 0)
        {
            return firstRowHeight
        }
        if indexPath.row == 8 {
            return tableView.frame.size.height - firstRowHeight - defaultRowHeight * rowCount
        }
        else
        {
            return defaultRowHeight
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //Save original photos
        if(indexPath.row == 1)
        {
            
        }
        //Autosave puzzle artwork
        else if(indexPath.row == 2)
        {
            
        }
        //Rate the app
        else if(indexPath.row == 4)
        {
            
        }
        //Terms of use
        else if(indexPath.row == 6)
        {
            
        }
        //Privacy policy
        else if(indexPath.row == 7)
        {
            
        }
    }
    
    func saveOriginalPhotosSwitchChanged(mySwitch: UISwitch) {
        USettingsData.sharedInstance.SaveOriginalPhotos = mySwitch.isOn
    }
    
    func savePuzzleArtworkSwitchChanged(mySwitch: UISwitch) {
        USettingsData.sharedInstance.SavePuzzleArtwork = mySwitch.isOn
    }
}

class SecretSettingsTableViewController: UITableViewController {
    
    @IBOutlet weak var centralImportanceSlider: UISlider!
    @IBOutlet weak var centralImportanceSliderLabel: UILabel!
    
    @IBOutlet weak var powerFunctionSlider: UISlider!
    @IBOutlet weak var powerFunctionSliderLabel: UILabel!
    
    @IBOutlet weak var refreshRateSlider: UISlider!
    @IBOutlet weak var refreshRateSliderLabel: UILabel!
    
    @IBOutlet weak var mainCycleSlider: UISlider!
    @IBOutlet weak var mainCycleSliderLabel: UILabel!
    
    @IBOutlet weak var setDefaultsButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        centralImportanceSlider.addTarget(self, action: #selector(centralImportanceSliderChanged), for: UIControlEvents.valueChanged)
        powerFunctionSlider.addTarget(self, action: #selector(powerFunctionSliderChanged), for: UIControlEvents.valueChanged)
        refreshRateSlider.addTarget(self, action: #selector(refreshRateSliderChanged), for: UIControlEvents.valueChanged)
        mainCycleSlider.addTarget(self, action: #selector(mainCycleSliderChanged), for: UIControlEvents.valueChanged)
        
        refreshSliderValues()
        
        setDefaultsButton.addTarget(self, action: #selector(setDefaultsButtonTap), for: UIControlEvents.touchUpInside)
        
        closeButton.addTarget(self, action: #selector(closeButtonTap), for: UIControlEvents.touchUpInside)
    }
    
    func centralImportanceSliderChanged(slider: UISlider) {
        USettingsData.sharedInstance.CentralImportance = Double(slider.value).roundTo(places: 1)
        centralImportanceSliderLabel.text = String(USettingsData.sharedInstance.CentralImportance)
        print(USettingsData.sharedInstance.CentralImportance)
    }
    
    func powerFunctionSliderChanged(slider: UISlider) {
        USettingsData.sharedInstance.PowerFunction = Double(slider.value).roundTo(places: 1)
        powerFunctionSliderLabel.text = String(USettingsData.sharedInstance.PowerFunction)
    }
    
    func refreshRateSliderChanged(slider: UISlider) {
        USettingsData.sharedInstance.RefreshRate = Int(slider.value)
        refreshRateSliderLabel.text = String(USettingsData.sharedInstance.RefreshRate)
    }
    
    func mainCycleSliderChanged(slider: UISlider) {
        USettingsData.sharedInstance.MainCycle = Int(slider.value)
        mainCycleSliderLabel.text = String(USettingsData.sharedInstance.MainCycle)
    }
    
    func setDefaultsButtonTap(button: UIButton) {
        USettingsData.sharedInstance.setSecretSettingsDefaults()
        refreshSliderValues()
    }
    
    func closeButtonTap(button: UIButton) {
        //self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func refreshSliderValues() {
        centralImportanceSlider.value = Float(USettingsData.sharedInstance.CentralImportance)
        centralImportanceSliderLabel.text = String(USettingsData.sharedInstance.CentralImportance)
        
        powerFunctionSlider.value = Float(USettingsData.sharedInstance.PowerFunction)
        powerFunctionSliderLabel.text = String(USettingsData.sharedInstance.PowerFunction)
        
        refreshRateSlider.value = Float(USettingsData.sharedInstance.RefreshRate)
        refreshRateSliderLabel.text = String(USettingsData.sharedInstance.RefreshRate)
        
        mainCycleSlider.value = Float(USettingsData.sharedInstance.MainCycle)
        mainCycleSliderLabel.text = String(USettingsData.sharedInstance.MainCycle)
        
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

